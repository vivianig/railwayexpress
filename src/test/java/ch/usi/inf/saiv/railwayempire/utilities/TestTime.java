package ch.usi.inf.saiv.railwayempire.utilities;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestTime {
    
    private final int startingYear = 1970;
    private Time time;
    
    @Before
    public void testBefore() {
        this.time = Time.getInstance();
        this.time.setStartingYear(this.startingYear);
        this.time.setTime(0);
        this.time.setSpeed(1);
    }
    
    @Test
    public void testToDate() {
        Assert.assertEquals("00:00:00 - January 1st, " + this.startingYear, this.time.toDate());
    }
    
    @Test
    public void testTimeAtZero() {
        
        Assert.assertEquals("Should have passed 0 milliseconds!", 0, this.time.getMilliseconds());
        Assert.assertEquals("Current milliseconds should be 0!", 0, this.time.getCurrentMillisecond());
        
        Assert.assertEquals("Should have passed 0 seconds!", 0, this.time.getSeconds());
        Assert.assertEquals("Current second should be 0!", 0, this.time.getCurrentSecond());
        
        Assert.assertEquals("Should have passed 0 minutes!", 0, this.time.getMinutes());
        Assert.assertEquals("Current minute should be 0!", 0, this.time.getCurrentMinute());
        
        Assert.assertEquals("Should have passed 0 hours!", 0, this.time.getHours());
        Assert.assertEquals("Current hours should be 0!", 0, this.time.getCurrentHour());
        
        Assert.assertEquals("Should have passed 0 days!", 0, this.time.getDays());
        Assert.assertEquals("Current days should be 1!", 1, this.time.getCurrentDay());
        
        Assert.assertEquals("Should have passed 0 month!", 0, this.time.getMonths());
        Assert.assertEquals("Current month should be 0!", 0, this.time.getCurrentMonth());
        
        Assert.assertEquals("Should have passed 0 years!", 0, this.time.getYears());
        Assert.assertEquals("Current second years be 1970!", this.startingYear, this.time.getCurrentYear());
    }
    
    @Test
    public void testTimeStep() {
        this.time.step(100);
        Assert.assertEquals("Should have passed 100 milliseconds!", Time.TIME_FACTOR*100, this.time.getMilliseconds());
    }
    
    @Test
    public void testTimeFactor() {
        this.time.setSpeed(2);
        this.time.step(100);
        Assert.assertEquals("Should have passed 200 milliseconds!", Time.TIME_FACTOR*100*2, this.time.getMilliseconds());
    }
    
    @Test
    public void testDelta() {
        this.time.step(100);
        Assert.assertEquals("Delta should be 100!", Time.TIME_FACTOR * 100, this.time.getDelta());
    }
    
    @Test
    public void testPausedDelta() {
        this.time.pause();
        Assert.assertEquals("Delta should be 0!", 0, this.time.getDelta());
        this.time.resume();
    }
    
    @Test
    public void testPaused() {
        Assert.assertFalse(this.time.isPaused());
        this.time.pause();
        Assert.assertTrue(this.time.isPaused());
        this.time.pause();
        Assert.assertTrue(this.time.isPaused());
        this.time.resume();
        Assert.assertFalse(this.time.isPaused());
        this.time.togglePause();
        Assert.assertTrue(this.time.isPaused());
        this.time.togglePause();
        Assert.assertFalse(this.time.isPaused());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testNegativeTimeFactor() {
        this.time.setSpeed(-2);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testNegativeSetTime() {
        this.time.setTime(-100);
    }
}
