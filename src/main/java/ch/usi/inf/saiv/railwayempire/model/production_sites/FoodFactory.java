package ch.usi.inf.saiv.railwayempire.model.production_sites;


import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.structures.StructureTypes;
import ch.usi.inf.saiv.railwayempire.model.wagons.FoodWagon;
import ch.usi.inf.saiv.railwayempire.model.wares.Food;
import ch.usi.inf.saiv.railwayempire.model.wares.IWare;
import ch.usi.inf.saiv.railwayempire.model.stores.IBuyable;
import ch.usi.inf.saiv.railwayempire.utilities.GameConstants;


/**
 * Food factory to produce food for people.
 */
public class FoodFactory extends AbstractProductionSite implements IBuyable {
    
    /**
     * serialVersionUID generated by eclipse.
     */
    private static final long serialVersionUID = -8378350201656573448L;
    /**
     * Food.
     */
    private final Food food;
    
    /**
     * Food factory.
     *
     * @param newX
     *            x coordinate.
     * @param newY
     *            y coordinate.
     */
    public FoodFactory(final int newX, final int newY) {
        super(newX,
            newY,
            "FACTORY_" + (int) (Math.random() * (4 - 1) + 1),
            GameConstants.FOOD_FACTORY_PRODUCTION_RATE,
            GameConstants.FOOD_FACTORY_MAX_UNIT);
        this.food = new Food(0.0);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public StructureTypes getStructureType() {
        return StructureTypes.FOOD_FACTORY;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public IWare getWare() {
        return this.food;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public double load(final FoodWagon wagon) {
        if (wagon.isFull()) {
            return 0;
        }
        
        final double toLoad;
        if (this.getWare().getQuantity() - wagon.getFreeSpace() < 0) {
            toLoad = this.getWare().getQuantity();
        } else {
            toLoad = wagon.getFreeSpace();
        }
        
        wagon.increaseAmountBy(toLoad);
        this.getWare().remove(toLoad);
        
        double cost = (int) (toLoad * this.getWare().getValue());
        if (this.isOwnedByPlayer()) {
            cost *= GameConstants.OWNERSHIP_REDUCTION;
        }
        Game.getInstance().getPlayer().subtractMoney((int) cost);
        
        return toLoad;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRemovable() {
        return false;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public int getCost() {
        return 7777777;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String getHoverText() {
        return "Food Factory";
    }
}
