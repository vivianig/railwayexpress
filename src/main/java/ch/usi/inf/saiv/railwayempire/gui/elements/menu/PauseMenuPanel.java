package ch.usi.inf.saiv.railwayempire.gui.elements.menu;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.controllers.GameplayController;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.elements.SimplePanel;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;
import ch.usi.inf.saiv.railwayempire.utilities.serialization.Save;

/**
 * Panel displaying the pause menu.
 * 
 */
public class PauseMenuPanel extends SimplePanel {
    /**
     * Color to hide the background.
     */
    private static final Color FADE_COLOR = new Color(0, 0, 0, 150);
    /**
     * Color of the background panel
     */
    private static final Color BACKGROUND_COLOR = new Color(57, 51, 46);
    /**
     * Height of the background panel.
     */
    private static final int BACKGROUND_HEIGHT = 250;
    /**
     * Width of the background panel.
     */
    private static final int BACKGROUND_WIDTH = 250;
    /**
     * Height of the pop up.
     */
    private static final int POPUP_HEIGHT = 150;
    /**
     * Width of the pop up.
     */
    private static final int POPUP_WIDTH = 500;
    /**
     * True if the pop up has to be rendered.
     */
    private boolean showExitPopup;
    /**
     * The pop up for when the user ask to exit.
     */
    private final ExitPopup exitPopup;
    /**
     * True if the pop up has to be rendered
     */
    private boolean showSellCompanyPopup;
    /**
     * The pop up for when the user ask to end the game.
     */
     private final SellCompanyPopup sellCompanyPopup;

     /**
      * Instantiate a new pause menu panel.
      * 
      * @param newShape
      *            The shape of the panel.
      * @param controller
      *            The controller handling the panel.
      * @param container
      *            The container of the panel.
      */
     public PauseMenuPanel(final Shape newShape, final GameplayController controller, final GameContainer container) {
         super(newShape);

         this.showExitPopup = false;
         this.showSellCompanyPopup = false;

         this.exitPopup = new ExitPopup(new Rectangle(this.getWidth() / 2 - PauseMenuPanel.POPUP_WIDTH / 2, this.getHeight() / 2
 - PauseMenuPanel.POPUP_HEIGHT / 2, PauseMenuPanel.POPUP_WIDTH,
                PauseMenuPanel.POPUP_HEIGHT), controller, container, this);
         this.sellCompanyPopup = new SellCompanyPopup(new Rectangle(this.getWidth() / 2 - PauseMenuPanel.POPUP_WIDTH / 2,
 this.getHeight() / 2
                        - PauseMenuPanel.POPUP_HEIGHT / 2, PauseMenuPanel.POPUP_WIDTH, PauseMenuPanel.POPUP_HEIGHT),
                controller, container, this);

         this.add(new SimplePanel(new Rectangle(SystemConstants.ZERO, SystemConstants.ZERO, this.getWidth(), this
                 .getHeight())));

         final Image resumeGameButtonImage = ResourcesLoader.getInstance().getImage("RESUME_GAME_BUTTON");
         final Image endGameButtonImage = ResourcesLoader.getInstance().getImage("SELL_COMPANY_BUTTON");
         final Image saveGameButtonImage = ResourcesLoader.getInstance().getImage("SAVE_GAME_BUTTON");
         final Image exitGameButtonImage = ResourcesLoader.getInstance().getImage("EXIT_GAME_BUTTON");

         final int buttonX = this.getWidth() / SystemConstants.TWO - endGameButtonImage.getWidth() / SystemConstants.TWO;
         final int buttonY = this.getHeight() / SystemConstants.TWO + 25;

         int rateoY = endGameButtonImage.getHeight() * 7 / 2;

         final Button resumeGameButton = new Button(resumeGameButtonImage, ResourcesLoader.getInstance().getImage(
                 "RESUME_GAME_BUTTON_HOVER"), ResourcesLoader.getInstance().getImage("RESUME_GAME_BUTTON_PRESSED"),
                 buttonX, buttonY - rateoY) {
         };

         rateoY -= endGameButtonImage.getHeight() * 3 / 2;

         final Button endGameButton = new Button(endGameButtonImage, ResourcesLoader.getInstance().getImage(
                 "SELL_COMPANY_BUTTON_HOVER"), ResourcesLoader.getInstance().getImage("SELL_COMPANY_BUTTON_PRESSED"),
                 buttonX,
                 buttonY - rateoY) {
         };

         rateoY -= endGameButtonImage.getHeight() * 3 / 2;

         final Button saveGameButton = new Button(saveGameButtonImage, ResourcesLoader.getInstance().getImage(
                 "SAVE_GAME_BUTTON_HOVER"), ResourcesLoader.getInstance().getImage("SAVE_GAME_BUTTON_PRESSED"),
                 buttonX,
                 buttonY - rateoY) {
         };

         rateoY -= endGameButtonImage.getHeight() * 3 / 2;

         final Button exitGameButton = new Button(exitGameButtonImage, ResourcesLoader.getInstance().getImage(
                 "EXIT_GAME_BUTTON_HOVER"), ResourcesLoader.getInstance().getImage("EXIT_GAME_BUTTON_PRESSED"),
                 buttonX,
                 buttonY - rateoY) {
         };

         resumeGameButton.setListener(new MouseListener() {

             @Override
             public void actionPerformed(final Button button) {
                 controller.togglePauseScreen();
                 Save.getInstance().setNeedToSaveAgain(true);
             }
         });

         endGameButton.setListener(new MouseListener() {

             @Override
             public void actionPerformed(final Button button) {
                 PauseMenuPanel.this.showSellCompanyPopup = !PauseMenuPanel.this.showSellCompanyPopup;
             }
         });
         saveGameButton.setListener(new MouseListener() {

             @Override
             public void actionPerformed(final Button button) {
                 controller.setSavePanel();
             }
         });
         exitGameButton.setListener(new MouseListener() {

             @Override
             public void actionPerformed(final Button button) {
                 if (Save.getInstance().needToSave()) {
                     PauseMenuPanel.this.exitPopup.getSaveButton().enableButton();
                     PauseMenuPanel.this.exitPopup.getLabel().setText(
                             "Are you sure you want to exit without saving your game?");
                 } else {
                     PauseMenuPanel.this.exitPopup.getSaveButton().disableButton();
                     PauseMenuPanel.this.exitPopup.getLabel().setText("Are you sure you want to exit?");
                 }
                 PauseMenuPanel.this.showExitPopup = !PauseMenuPanel.this.showExitPopup;
             }
         });

         this.add(resumeGameButton);
         this.add(endGameButton);
         this.add(saveGameButton);
         this.add(exitGameButton);

     }

     @Override
     public void render(final GUIContext container, final Graphics graphics) {
         graphics.setColor(PauseMenuPanel.FADE_COLOR);
         graphics.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
         graphics.setColor(PauseMenuPanel.BACKGROUND_COLOR);
         graphics.fillRoundRect(this.getWidth() / 2 - PauseMenuPanel.BACKGROUND_WIDTH / 2, this.getHeight() / 2
                - PauseMenuPanel.BACKGROUND_HEIGHT
                 / 2,
 PauseMenuPanel.BACKGROUND_WIDTH,
                PauseMenuPanel.BACKGROUND_HEIGHT, 15);
         graphics.setColor(Color.black);
         graphics.drawRoundRect(this.getWidth() / 2 - PauseMenuPanel.BACKGROUND_WIDTH / 2, this.getHeight() / 2
                - PauseMenuPanel.BACKGROUND_HEIGHT
                 / 2,
 PauseMenuPanel.BACKGROUND_WIDTH - 1,
                PauseMenuPanel.BACKGROUND_HEIGHT - 1, 15);
         super.render(container, graphics);
         if (this.showExitPopup) {
             this.exitPopup.render(container, graphics);
         } else if (this.showSellCompanyPopup) {
             this.sellCompanyPopup.render(container, graphics);
         }
     }

     /**
      * Toggle the exit pop up.
      */
     public void toggleExitPopup() {
         this.showExitPopup = !this.showExitPopup;
     }

    /**
     * Toggle the sell company pop up.
     */
     public void toggleSellCompanyPopup() {
         this.showSellCompanyPopup = !this.showSellCompanyPopup;
     }

     /**
      * {@inheritDoc}
      */
     @Override
     public void mouseClicked(final int button, final int coordX, final int coordY, final int clickCount) {
         if (this.showExitPopup) {
             this.exitPopup.mouseClicked(button, coordX, coordY, clickCount);
         } else if (this.showSellCompanyPopup) {
             this.sellCompanyPopup.mouseClicked(button, coordX, coordY, clickCount);
         } else {
             super.mouseClicked(button, coordX, coordY, clickCount);
         }
     }

     /**
      * {@inheritDoc}
      */
     @Override
     public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
         if (this.showExitPopup) {
             this.exitPopup.mouseMoved(oldX, oldY, newX, newY);
         } else if (this.showSellCompanyPopup) {
             this.sellCompanyPopup.mouseMoved(oldX, oldY, newX, newY);
         } else {
             super.mouseMoved(oldX, oldY, newX, newY);
         }
     }

     /**
      * {@inheritDoc}
      */
     @Override
     public void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
         if (this.showExitPopup) {
             this.exitPopup.mouseDragged(oldX, oldY, newX, newY);
         } else if (this.showSellCompanyPopup) {
             this.sellCompanyPopup.mouseDragged(oldX, oldY, newX, newY);
         } else {
             super.mouseDragged(oldX, oldY, newX, newY);
         }
     }

     /**
      * {@inheritDoc}
      */
     @Override
     public void mousePressed(final int button, final int coordX, final int coordY) {
         if (this.showExitPopup) {
             this.exitPopup.mousePressed(button, coordX, coordY);
         } else if (this.showSellCompanyPopup) {
             this.sellCompanyPopup.mousePressed(button, coordX, coordY);
         } else {
             super.mousePressed(button, coordX, coordY);
         }
     }

     /**
      * {@inheritDoc}
      */
     @Override
     public void mouseReleased(final int button, final int coordX, final int coordY) {
         if (this.showExitPopup) {
             this.exitPopup.mouseReleased(button, coordX, coordY);
         } else if (this.showSellCompanyPopup) {
             this.sellCompanyPopup.mouseReleased(button, coordX, coordY);
         } else {
             super.mouseReleased(button, coordX, coordY);
         }
     }

     /**
      * {@inheritDoc}
      */
     @Override
     public void mouseWheelMoved(final int charge, final int coordX, final int coordY) {
         if (this.showExitPopup) {
             this.exitPopup.mouseWheelMoved(charge, coordX, coordY);
         } else if (this.showSellCompanyPopup) {
             this.sellCompanyPopup.mouseWheelMoved(charge, coordX, coordY);
         } else {
             super.mouseWheelMoved(charge, coordX, coordY);
         }
     }

}
