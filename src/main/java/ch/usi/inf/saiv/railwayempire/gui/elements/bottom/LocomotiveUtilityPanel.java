package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.controllers.GameModesController;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.Label;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.viewers.CoordinatesManager;
import ch.usi.inf.saiv.railwayempire.model.trains.Locomotive;
import ch.usi.inf.saiv.railwayempire.model.trains.Train;
import ch.usi.inf.saiv.railwayempire.utilities.GraphicsUtils;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Bottom Panel for utility display according to selected locomotive if selected at all.
 */
public final class LocomotiveUtilityPanel extends AbstractWorldInfoUtilityPanel {
    
    /**
     * Label width.
     */
    private final static int LABEL_WIDTH = 180;
    /**
     * Label height.
     */
    private final static int LABEL_HEIGHT = 20;
    /**
     * The margin for the logo.
     */
    private final static int IMAGE_MARGIN = 8;
    /**
     * Margin.
     */
    private final static int MARGIN = 5;
    /**
     * The image of the logo.
     */
    private final Button locomotiveLogo;
    /**
     * Selected locomotives train.
     */
    private Train train;
    
    /**
     * Creates the panel for displaying selected locomotive information.
     *
     * @param rectangle
     *            utility rectangle.
     * @param locomotive
     *            selected locomotive.
     * @param modesController
     *            modes controller.
     */
    public LocomotiveUtilityPanel(final Rectangle rectangle, final Locomotive locomotive,
        final GameModesController modesController) {
        super(rectangle);
        
        for (final Train train : Game.getInstance().getWorld().getTrainManager().getTrains()) {
            if (train.getLocomotive().equals(locomotive)) {
                this.train = train;
            }
        }
        
        final ResourcesLoader loader = ResourcesLoader.getInstance();
        
        this.locomotiveLogo =
            new Button(loader.getImage(locomotive.getTextureName() + "_BUTTON"),
                loader.getImage(locomotive.getTextureName() + "_BUTTON_HOVER"),
                loader.getImage(locomotive.getTextureName() + "_BUTTON_PRESSED"),
                this.getX(),
                this.getY());
        this.locomotiveLogo.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                CoordinatesManager.getInstance().centerCameraOn(LocomotiveUtilityPanel.this.train.getX(),
                    LocomotiveUtilityPanel.this.train.getY());
            }
        });
        
        this.add(this.locomotiveLogo);
        
        this.add(new Label(this.getX() + LocomotiveUtilityPanel.MARGIN + this.locomotiveLogo.getWidth() + 2
            * LocomotiveUtilityPanel.IMAGE_MARGIN,
            this.getY(),
            LocomotiveUtilityPanel.LABEL_WIDTH,
            LocomotiveUtilityPanel.LABEL_HEIGHT,
            locomotive.getName()));
        
        this.add(new Label(this.getX() + LocomotiveUtilityPanel.MARGIN + this.locomotiveLogo.getWidth() + 2
            * LocomotiveUtilityPanel.IMAGE_MARGIN,
            this.getY() + LocomotiveUtilityPanel.MARGIN + LocomotiveUtilityPanel.LABEL_HEIGHT,
            LocomotiveUtilityPanel.LABEL_WIDTH,
            LocomotiveUtilityPanel.LABEL_HEIGHT,
            Integer.toString(locomotive.getCost())));
        
        this.add(new Label(this.getX() + LocomotiveUtilityPanel.MARGIN + this.locomotiveLogo.getWidth() + 2
            * LocomotiveUtilityPanel.IMAGE_MARGIN,
            this.getY() + LocomotiveUtilityPanel.MARGIN + LocomotiveUtilityPanel.LABEL_HEIGHT * 2,
            LocomotiveUtilityPanel.LABEL_WIDTH,
            LocomotiveUtilityPanel.LABEL_HEIGHT,
            null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                final String status = "Status: " + LocomotiveUtilityPanel.this.train.getState();
                GraphicsUtils.drawShadowText(graphics, status, this.getX(), this.getY());
            }
        });
        
        this.add(new Label(this.getX() + LocomotiveUtilityPanel.MARGIN + this.locomotiveLogo.getWidth() + 2
            * LocomotiveUtilityPanel.IMAGE_MARGIN,
            this.getY() + LocomotiveUtilityPanel.MARGIN + LocomotiveUtilityPanel.LABEL_HEIGHT * 3,
            LocomotiveUtilityPanel.LABEL_WIDTH,
            LocomotiveUtilityPanel.LABEL_HEIGHT,
            null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                final String currentSpeed = "Current Speed: " + LocomotiveUtilityPanel.this.train.getVelocityString();
                GraphicsUtils.drawShadowText(graphics, currentSpeed, this.getX(), this.getY());
            }
        });
        
        this.add(new Label(this.getX() + LocomotiveUtilityPanel.MARGIN + this.locomotiveLogo.getWidth() + 2
            * LocomotiveUtilityPanel.IMAGE_MARGIN,
            this.getY() + LocomotiveUtilityPanel.MARGIN + LocomotiveUtilityPanel.LABEL_HEIGHT * 4,
            LocomotiveUtilityPanel.LABEL_WIDTH,
            LocomotiveUtilityPanel.LABEL_HEIGHT, null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                final String topSpeed = "Top speed: " + locomotive.getVelocityString();
                GraphicsUtils.drawShadowText(graphics, topSpeed, this.getX(), this.getY());
            }
        });
        
        this.add(new Label(this.getX() + LocomotiveUtilityPanel.MARGIN + this.locomotiveLogo.getWidth() + 2
            * LocomotiveUtilityPanel.IMAGE_MARGIN,
            this.getY() + LocomotiveUtilityPanel.MARGIN + LocomotiveUtilityPanel.LABEL_HEIGHT * 5,
            LocomotiveUtilityPanel.LABEL_WIDTH,
            LocomotiveUtilityPanel.LABEL_HEIGHT,
            "Wagon limit: " + locomotive.getWagonLimit()));
        
        this.add(new Label(this.getX() + LocomotiveUtilityPanel.MARGIN + this.locomotiveLogo.getWidth() + 2
            * LocomotiveUtilityPanel.IMAGE_MARGIN,
            this.getY() + LocomotiveUtilityPanel.MARGIN + LocomotiveUtilityPanel.LABEL_HEIGHT * 6,
            LocomotiveUtilityPanel.LABEL_WIDTH,
            LocomotiveUtilityPanel.LABEL_HEIGHT,
            null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                final String currentLimit =
                    "Wagons currently pulling: " + LocomotiveUtilityPanel.this.train.getWagons().size();
                GraphicsUtils.drawShadowText(graphics, currentLimit, this.getX(), this.getY());
            }
        });
        
        this.add(new Label(this.getX() + LocomotiveUtilityPanel.MARGIN + this.locomotiveLogo.getWidth() + 2
            * LocomotiveUtilityPanel.IMAGE_MARGIN,
            this.getY() + LocomotiveUtilityPanel.MARGIN + LocomotiveUtilityPanel.LABEL_HEIGHT * 7,
            LocomotiveUtilityPanel.LABEL_WIDTH,
            LocomotiveUtilityPanel.LABEL_HEIGHT,
            null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                final String nextStop =
                    "Next stop: " + LocomotiveUtilityPanel.this.train.getRoute().getCurrentStation().getName();
                GraphicsUtils.drawShadowText(graphics, nextStop, this.getX(), this.getY());
            }
        });
    }
}
