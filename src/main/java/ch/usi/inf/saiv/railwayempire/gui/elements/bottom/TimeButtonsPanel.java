package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.geom.Rectangle;

import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.IGuiComponent;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.elements.SimplePanel;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;
import ch.usi.inf.saiv.railwayempire.utilities.Time;

/**
 * Class for the four time control buttons.
 */
public class TimeButtonsPanel extends SimplePanel {
    /**
     * The margin between buttons.
     */
    private static final int BUTTONS_MARGIN = 5;
    /**
     * The width of a small button.
     */
    private static final int BUTTON_WIDTH = ResourcesLoader.getInstance().getImage("NORMAL_MODE_BUTTON").getWidth();
    /**
     * The height of a small button.
     */
    private static final int BUTTON_HEIGHT = ResourcesLoader.getInstance().getImage("NORMAL_MODE_BUTTON").getHeight();
    
    /**
     * Constructor.
     * 
     * @param posX
     *            x coordinate.
     * @param posY
     *            y coordinate.
     */
    public TimeButtonsPanel(final int posX, final int posY) {
        super(new Rectangle(posX,
            posY,
            TimeButtonsPanel.BUTTON_WIDTH * 4 + TimeButtonsPanel.BUTTONS_MARGIN * 5,
            TimeButtonsPanel.BUTTON_HEIGHT + TimeButtonsPanel.BUTTONS_MARGIN));
        
        final ResourcesLoader loader = ResourcesLoader.getInstance();
        
        final Button pauseButton = new Button(
            loader.getImage("TIME_PAUSE_BUTTON"),
            loader.getImage("TIME_PAUSE_BUTTON_HOVER"),
            loader.getImage("TIME_PAUSE_BUTTON_PRESSED"),
            this.getHorizontalPosition(1),
            this.getY() + TimeButtonsPanel.BUTTONS_MARGIN);
        pauseButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                TimeButtonsPanel.this.releaseAllButtons();
                pauseButton.holdButton();
                Time.getInstance().setSpeed(SystemConstants.ZERO);
            }
        });
        
        final Button normalButton = new Button(
            loader.getImage("NORMAL_SPEED_BUTTON"),
            loader.getImage("NORMAL_SPEED_BUTTON_HOVER"),
            loader.getImage("NORMAL_SPEED_BUTTON_PRESSED"),
            this.getHorizontalPosition(2),
            this.getY() + TimeButtonsPanel.BUTTONS_MARGIN);
        normalButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                TimeButtonsPanel.this.releaseAllButtons();
                normalButton.holdButton();
                Time.getInstance().setSpeed(SystemConstants.ONE);
            }
        });
        
        final Button fastButton = new Button(loader.getImage("FAST_SPEED_BUTTON"),
            loader.getImage("FAST_SPEED_BUTTON_HOVER"),
            loader.getImage("FAST_SPEED_BUTTON_PRESSED"),
            this.getHorizontalPosition(3),
            this.getY() + TimeButtonsPanel.BUTTONS_MARGIN);
        fastButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                TimeButtonsPanel.this.releaseAllButtons();
                fastButton.holdButton();
                Time.getInstance().setSpeed(SystemConstants.TWO);
            }
        });
        
        final Button fasterButton = new Button(loader.getImage("FASTER_SPEED_BUTTON"),
            loader.getImage("FASTER_SPEED_BUTTON_HOVER"),
            loader.getImage("FASTER_SPEED_BUTTON_PRESSED"),
            this.getHorizontalPosition(4),
            this.getY() + TimeButtonsPanel.BUTTONS_MARGIN);
        fasterButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                TimeButtonsPanel.this.releaseAllButtons();
                fasterButton.holdButton();
                Time.getInstance().setSpeed(5);
            }
        });
        
        this.add(pauseButton);
        this.add(normalButton);
        this.add(fastButton);
        this.add(fasterButton);
        normalButton.holdButton();
        
        this.setBackground(loader.getImage("BACKGROUND"));
    }
    
    /**
     * Returns the vertical position of the button given the button number.
     * 
     * @param buttonNumber
     *            The number of the button.
     * @return The horizontal offset.
     */
    private int getHorizontalPosition(final int buttonNumber) {
        return this.getX() + TimeButtonsPanel.BUTTONS_MARGIN
            * buttonNumber
            + TimeButtonsPanel.BUTTON_WIDTH
            * (buttonNumber - 1);
    }
    
    /**
     * Unhold all buttons.
     */
    private void releaseAllButtons() {
        for (final IGuiComponent button : this.getChildrenComponents()) {
            ((Button) button).unHoldButton();
        }
    }
}
