package ch.usi.inf.saiv.railwayempire.gui.elements.center;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.controllers.GameModes;
import ch.usi.inf.saiv.railwayempire.controllers.GameModesController;
import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractGuiComponent;
import ch.usi.inf.saiv.railwayempire.gui.modes.IMode;
import ch.usi.inf.saiv.railwayempire.gui.viewers.CoordinatesManager;
import ch.usi.inf.saiv.railwayempire.gui.viewers.LogViewer;
import ch.usi.inf.saiv.railwayempire.gui.viewers.WorldViewer;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.production_sites.IProductionSite;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.utilities.IPersistent;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;

/**
 * World panel. holds a world viewer and the current mode.
 */
public final class WorldPanel extends AbstractGuiComponent implements IPersistent {
    
    /** World viewer. */
    private final WorldViewer worldViewer;
    /** Log viewer. */
    private final LogViewer logViewer;
    /** Current active mode. */
    private IMode currentMode;
    /** Controller for switching the modes. */
    private final GameModesController modesController;
    /** True if right mouse button is pressed. */
    private boolean rightButton;
    
    /**
     * Constructor.
     *
     * @param rectangle
     *            shape of world panel.
     * @param modesController
     *            modes controller.
     */
    public WorldPanel(final Rectangle rectangle, final GameModesController modesController) {
        super(rectangle);
        Game.addPersistent(this);
        this.worldViewer = new WorldViewer();
        this.logViewer = LogViewer.getInstance();
        this.modesController = modesController;
        this.modesController.initWorldPanel(rectangle, this);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        graphics.setClip((Rectangle) this.getShape());
        this.worldViewer.render(container, graphics);
        this.currentMode.render(container, graphics);
        this.logViewer.render(container, graphics);
        graphics.clearClip();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final int button, final int coordX, final int coordY, final int clickCount) {
        this.currentMode.mouseClicked(button, coordX, coordY, clickCount);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final int button, final int coordX, final int coordY) {
        this.currentMode.mousePressed(button, coordX, coordY);
        if (button == SystemConstants.MOUSE_BUTTON_RIGHT) {
            this.rightButton = true;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final int button, final int coordX, final int coordY) {
        this.currentMode.mouseReleased(button, coordX, coordY);
        if (button == SystemConstants.MOUSE_BUTTON_RIGHT) {
            this.rightButton = false;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        if (this.rightButton) {
            CoordinatesManager.getInstance().updateOffset(newX - oldX, newY - oldY);
        } else {
            this.currentMode.mouseDragged(oldX, oldY, newX, newY);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        this.currentMode.mouseMoved(oldX, oldY, newX, newY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseWheelMoved(final int charge, final int coordX, final int coordY) {
        if (charge > SystemConstants.ZERO) {
            CoordinatesManager.getInstance().zoomIn();
        } else {
            CoordinatesManager.getInstance().zoomOut();
        }
        this.currentMode.mouseWheelMoved(charge, coordX, coordY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void keyPressed(final int key, final char character) {
        this.currentMode.keyPressed(key, character);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void reload() {
        this.modesController.enterMode(GameModes.NORMAL);
    }
    
    /**
     * Set current mode.
     *
     * @param newCurrentMode
     *            new mode.
     */
    public void setCurrentMode(final IMode newCurrentMode) {
        this.currentMode = newCurrentMode;
    }
    
    /**
     * Get current mode.
     *
     * @return current mode.
     */
    public IMode getCurrentMode() {
        return this.currentMode;
    }
    
    /**
     * Set transparency of mountains.
     *
     * @param value
     *            true for transparent mountains, false for default.
     */
    public void setMountainTransparent(final boolean value) {
        this.worldViewer.setMountainTransparent(value);
    }
    
    /**
     * Sets the radius of the station highlighted, visible.
     *
     * @param station
     *            selected station.
     */
    public void enableStationRadius(final Station station) {
        this.worldViewer.enableStationRadius(station);
    }
    
    /**
     * Set selected production site.
     * 
     * @param productionSite
     *            production site.
     */
    public void highlightSelected(final IProductionSite productionSite) {
        this.worldViewer.highlightSelected(productionSite);
    }
}
