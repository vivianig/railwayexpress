/**
 * Provides the user with the option to buy train components.
 */
package ch.usi.inf.saiv.railwayempire.model.stores;