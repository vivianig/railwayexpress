package ch.usi.inf.saiv.railwayempire.gui.elements.menu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Class to fetch and hold the informations about a save game file
 */
public class SaveGameInfo {

    /**
     * Name of the player.
     */
    private String playerName;
    /**
     * Name of the company of the player.
     */
    private String companyName;
    /**
     * Money of the player.
     */
    private String money;
    /**
     * Time of the game.
     */
    private final String time;
    /**
     * Number of trains owned by the player.
     */
    private String ownerTrains;
    /**
     * Number of stations owned by the player.
     */
    private String ownerStation;

    /**
     * Construct. Read all the informations from the file and stores them.
     * 
     * @param fileName
     *            the name of the file.
     * @throws IOException
     *             can throw an io exception in case the file doesn't exist,
     *             it's corrupted, or it doesn't respect the default template.
     */
    public SaveGameInfo(final String fileName) throws IOException {

        this.playerName = "Corrupted data";
        this.companyName = "Corrupted data";
        this.money = "Corrupted data";
        this.ownerTrains = "Corrupted data";
        this.ownerStation = "Corrupted data";
        final FileReader file = new FileReader(new File("saves/" + fileName + ".pd"));
        final BufferedReader bf = new BufferedReader(file);
        this.playerName = bf.readLine();
        this.companyName = bf.readLine();
        this.money = bf.readLine();
        this.time = bf.readLine();
        this.ownerTrains = bf.readLine();
        this.ownerStation = bf.readLine();
        bf.close();
    }

    /**
     * Return the player name.
     * 
     * @return the player name.
     */
    public String getPlayerName() {
        return this.playerName;
    }

    /**
     * Return the company name.
     * 
     * @return the company name.
     */
    public String getCompanyName() {
        return this.companyName;
    }

    /**
     * Return the money.
     * 
     * @return the money.
     */
    public String getMoney() {
        return this.money;
    }

    /**
     * Return the time.
     * 
     * @return the time.
     */
    public String getTime() {
        return this.time;
    }

    /**
     * Return the number of owned trains.
     * 
     * @return the number of owned trains.
     */
    public String getOwnedTrains() {
        return this.ownerTrains;
    }

    /**
     * Return the number of owned stations.
     * 
     * @return the number of owned stations.
     */
    public String getOwnedStations() {
        return this.ownerStation;
    }

}
