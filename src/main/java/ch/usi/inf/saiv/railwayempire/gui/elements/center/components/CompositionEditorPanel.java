package ch.usi.inf.saiv.railwayempire.gui.elements.center.components;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.AbstractComponent;
import org.newdawn.slick.gui.ComponentListener;
import org.newdawn.slick.gui.GUIContext;
import org.newdawn.slick.gui.TextField;

import ch.usi.inf.saiv.railwayempire.controllers.TrainSelectionController;
import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.Label;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.elements.TrainDisplayPanel;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.generators.NamesGenerator;
import ch.usi.inf.saiv.railwayempire.model.trains.Locomotive;
import ch.usi.inf.saiv.railwayempire.model.trains.Train;
import ch.usi.inf.saiv.railwayempire.model.trains.WareHouse;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils.Alignment;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Display the train being composed.
 */
public class CompositionEditorPanel extends AbstractPanel {
    
    /**
     * The padding of the panel.
     */
    private static final int PADDING = 10;
    
    private final TextField trainNameField;
    
    /**
     * The train to display.
     */
    private Train train;
    
    /**
     * THe ware house of the player containing his trains.
     */
    private final WareHouse wareHouse;
    
    /**
     * Button to create a new train.
     */
    private final Button newTrainButton;
    
    /**
     * Button to dismantle train
     */
    private final Button destroyTrainButton;
    
    private final Button renameButton;
    
    private final TrainDisplayPanel trainComposition;
    
    private final Label noTrainLabel;
    
    /**
     * Create a panel to display the train
     * 
     * @param newShape
     *            rectangle of the panel
     */
    protected CompositionEditorPanel(final Rectangle newShape,
        final TrainSelectionController controller,
        final GameContainer container) {
        super(newShape);
        this.wareHouse = Game.getInstance().getPlayer().getWareHouse();
        
        final ResourcesLoader loader = ResourcesLoader.getInstance();
        
        // Destroy Train Button
        this.destroyTrainButton = new Button(loader.getImage("DISASSEMBLE_TRAIN_BUTTON"),
            loader.getImage("DISASSEMBLE_TRAIN_BUTTON_HOVER"),
            loader.getImage("DISASSEMBLE_TRAIN_BUTTON_PRESSED"),
            loader.getImage("DISASSEMBLE_TRAIN_BUTTON_DISABLED"),
            0, 0);
        this.destroyTrainButton.setLocation(this.getX() + this.getWidth() - CompositionEditorPanel.PADDING
            - this.destroyTrainButton.getWidth(),
            this.getY() + this.getHeight() - this.destroyTrainButton.getHeight() - CompositionEditorPanel.PADDING);
        this.destroyTrainButton.setListener(new MouseListener() {
            
            /**
             * {@inheritDoc}
             */
            @Override
            public void actionPerformed(final Button button) {
                if (CompositionEditorPanel.this.train == null) {
                    return;
                }
                if (!CompositionEditorPanel.this.train.isParked()) {
                    Game.getInstance().getWorld().getTrainManager().removeTrain(CompositionEditorPanel.this.train);
                }
                CompositionEditorPanel.this.wareHouse.removeTrain(CompositionEditorPanel.this.train);
                controller.notifyTrainRemoved(CompositionEditorPanel.this.train);
                
            }
        });
        
        this.add(this.destroyTrainButton);
        
        // New Train Button
        this.newTrainButton = new Button(loader.getImage("NEW_TRAIN_BUTTON"),
            loader.getImage("NEW_TRAIN_BUTTON_HOVER"),
            loader.getImage("NEW_TRAIN_BUTTON_PRESSED"),
            loader.getImage("NEW_TRAIN_BUTTON_DISABLED"), 0, 0);
        
        this.newTrainButton.setLocation(this.destroyTrainButton.getX()
            - CompositionEditorPanel.PADDING - this.newTrainButton.getWidth(), this.destroyTrainButton.getY());
        
        this.newTrainButton.setListener(new MouseListener() {
            
            /**
             * {@inheritDoc}
             */
            @Override
            public void actionPerformed(final Button button) {
                controller.notifyUnSelected();
            }
        });
        
        this.add(this.newTrainButton);
        
        // Rename Button
        this.renameButton = new Button(loader.getImage("RENAME_TRAIN_BUTTON"),
            loader.getImage("RENAME_TRAIN_BUTTON_HOVER"),
            loader.getImage("RENAME_TRAIN_BUTTON_PRESSED"),
            loader.getImage("RENAME_TRAIN_BUTTON_DISABLED"), 0, 0);
        this.renameButton.setLocation(this.newTrainButton.getX() - CompositionEditorPanel.PADDING
            - this.renameButton.getWidth(), this.destroyTrainButton.getY());
        
        this.renameButton.setListener(new MouseListener() {
            
            /**
             * {@inheritDoc}
             */
            @Override
            public void actionPerformed(final Button button) {
                CompositionEditorPanel.this.renameTrain();
                
            }
        });
        
        this.add(this.renameButton);
        
        // Train Name Text Field
        this.trainNameField = new TextField(container,
            container.getGraphics().getFont(),
            this.getX() + 20,
            this.getY() + 20,
            container.getGraphics().getFont().getWidth("M") * 23, 25,
            new ComponentListener() {
                
                /**
                 * {@inheritDoc}
                 */
                @Override
                public void componentActivated(final AbstractComponent arg0) {
                    CompositionEditorPanel.this.renameTrain();
                    
                }
            });
        this.trainNameField.setBorderColor(null);
        
        //this.trainNameField.setBackgroundColor(null);
        this.trainNameField.setLocation(this.renameButton.getX() - CompositionEditorPanel.PADDING
            - this.trainNameField.getWidth(),
            this.renameButton.getY() + (this.renameButton.getHeight() - this.trainNameField.getHeight()) / 2);
        this.trainNameField.setMaxLength(32);
        
        final Label trainNameLabel = new Label(this.getX(), this.renameButton.getY(),
            this.trainNameField.getX() - this.getX() - CompositionEditorPanel.PADDING,
            this.renameButton.getHeight(), "Train Name", Alignment.RIGHT, true);
        this.add(trainNameLabel);
        
        this.trainComposition = new TrainDisplayPanel(new Rectangle(this.getX() + CompositionEditorPanel.PADDING,
            this.getY() + CompositionEditorPanel.PADDING + 24,
            this.getWidth() - CompositionEditorPanel.PADDING * 2,
            this.destroyTrainButton.getY() - this.getY() - CompositionEditorPanel.PADDING * 2 - 44), null);
        
        this.add(this.trainComposition);
        
        this.noTrainLabel = new Label(this.trainComposition.getX(), this.trainComposition.getY(),
            this.trainComposition.getWidth(), this.trainComposition.getHeight(),
            "Add a locomotive to start a new train.", Alignment.CENTER, true);
        this.noTrainLabel.setFont(FontUtils.LARGE_FONT);
        
        //The train needs to be initialized after the buttons
        this.setTrain(null);
        
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        super.render(container, graphics);
        if (this.train == null) {
            graphics.setColor(graphics.getColor().multiply(new Color(0, 0, 0, 0.3f)));
            graphics.fillRect(this.trainNameField.getX(),
                this.trainNameField.getY(),
                this.trainNameField.getWidth(),
                this.trainNameField.getHeight());
            this.noTrainLabel.render(container, graphics);
        } else {
            this.trainNameField.render(container, graphics);
        }
    }
    
    /**
     * @return the train
     */
    public Train getTrain() {
        return this.train;
    }
    
    /**
     * @param train
     *            the train to set
     */
    public void setTrain(final Train train) {
        if (null == train) {
            this.newTrainButton.disableButton();
            this.trainNameField.deactivate();
            this.renameButton.disableButton();
            this.destroyTrainButton.disableButton();
        } else if (null == this.train) {
            this.newTrainButton.enableButton();
            this.trainNameField.setFocus(true);
            this.renameButton.enableButton();
            this.destroyTrainButton.enableButton();
        }
        this.train = train;
        this.trainComposition.setTrain(train);
        this.trainNameField.setText(train == null ? "" : this.train.getName());
        this.trainNameField.setCursorPos(this.trainNameField.getText().length());
        
    }
    
    /**
     * Rename the train using the value in the trainNameField.
     */
    private void renameTrain() {
        if (this.train != null) {
            this.train.setName(this.trainNameField.getText());
        } else {
            this.trainNameField.setText("");
        }
    }
    
    /**
     * Create a new train with the given locomotive.
     * 
     * @param locomotive
     *            The locomotive for the new train.
     */
    public void createNewTrain(final Locomotive locomotive) {
        this.wareHouse.addTrain(new Train(NamesGenerator.getInstance().generateTrainName(), locomotive));
        
    }
}
