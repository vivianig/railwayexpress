package ch.usi.inf.saiv.railwayempire.gui.viewers;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.Terrain;
import ch.usi.inf.saiv.railwayempire.model.cells.CellTypes;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.cells.SeaCell;
import ch.usi.inf.saiv.railwayempire.utilities.IPersistent;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Class that takes care of drawing transitions between tile texture types and heights.
 */
public final class TransitionViewer implements IPersistent{
    
    /** Connection value of TOP_EDGE. */
    private static final int TOP_EDGE_FROM_256 = 1;
    /** Connection value of TOP_LEFT_CORNER. */
    private static final int TOP_LEFT_CORNER_FROM_256 = 2;
    /** Connection value of RIGHT_EDGE. */
    private static final int RIGHT_EDGE_FROM_256 = 4;
    /** Connection value of BOTTOM_RIGHT_CORNER. */
    private static final int BOTTOM_RIGHT_CORNER_FROM_256 = 8;
    /** Connection value of BOTTOM_EDGE. */
    private static final int BOTTOM_EDGE_FROM_256 = 16;
    /** Connection value of BOTTOM_LEFT_CORNER. */
    private static final int BOTTOM_LEFT_CORNER_FROM_256 = 32;
    /** Connection value of TOP_RIGHT_CORNER. */
    private static final int TOP_RIGHT_CORNER_FROM_256 = 64;
    /** Connection value of LEFT_EDGE. */
    private static final int LEFT_EDGE_FROM_256 = 128;
    /** Connection value of TOP_EDGE. */
    private static final int TOP_EDGE = 1;
    /** Connection value of RIGHT_EDGE. */
    private static final int RIGHT_EDGE = 8;
    /** Connection value of BOTTOM_EDGE. */
    private static final int BOTTOM_EDGE = 4;
    /** Connection value of LEFT_EDGE. */
    private static final int LEFT_EDGE = 2;
    /** The Resource loader. */
    private final ResourcesLoader resourcesLoader;
    /** The instance of the resource loader. */
    private final CoordinatesManager coordinatesManager;
    /** Terrain. */
    private Terrain terrain;
    
    /**
     * Constructor.
     */
    public TransitionViewer() {
        this.terrain = Game.getInstance().getWorld().getTerrain();
        this.coordinatesManager = CoordinatesManager.getInstance();
        this.resourcesLoader = ResourcesLoader.getInstance();
        this.doAutoTransition();
    }
    
    /**
     * Returns the value corresponding to the neighbor of different type which are connected
     * on the edges. Method used for depth1 and depth2 sea transition to ground.
     * It has 256 possibilities computed by the following table.<code><br>
     *        | 1 |   <br>
     *     ---+---+--- <br>
     * &nbsp2 |   | 8 <br>
     *     ---+---+---<br>
     *        | 4 |   <br>
     * </code>
     *
     * @param posY
     *            cell x position.
     * @param posX
     *            cell y position.
     * @param currentCellType
     *            cell type.
     * @return transition value, if one of the cells are not of the same type, it returns 0.
     */
    private int computeTransistionDifferentType(final int posY, final int posX,
        final CellTypes currentCellType) {
        int temp = 0;
        if (posY > 0) {
            final ICell nextCell = this.terrain.getCell(posY - 1, posX);
            if (nextCell.getCellType() == CellTypes.GROUND) {
                temp += TransitionViewer.TOP_EDGE_FROM_256;
            }
        }
        if (posX < this.terrain.getSize() - 1) {
            final ICell nextCell = this.terrain.getCell(posY, posX + 1);
            if (nextCell.getCellType() == CellTypes.GROUND) {
                temp += TransitionViewer.RIGHT_EDGE_FROM_256;
            }
        }
        if (posY < this.terrain.getSize() - 1) {
            final ICell nextCell = this.terrain.getCell(posY + 1, posX);
            if (nextCell.getCellType() == CellTypes.GROUND) {
                temp += TransitionViewer.BOTTOM_EDGE_FROM_256;
            }
        }
        if (posX > 0) {
            final ICell nextCell = this.terrain.getCell(posY, posX - 1);
            if (nextCell.getCellType() == CellTypes.GROUND) {
                temp += TransitionViewer.LEFT_EDGE_FROM_256;
            }
        }
        if (posX > 0 && posY > 0) {
            final ICell nextCell = this.terrain.getCell(posY - 1, posX - 1);
            if (nextCell.getCellType() == CellTypes.GROUND) {
                temp += TransitionViewer.TOP_LEFT_CORNER_FROM_256;
            }
        }
        if (posX > 0 && posY > 0) {
            final ICell nextCell = this.terrain.getCell(posY - 1, posX + 1);
            if (nextCell.getCellType() == CellTypes.GROUND) {
                temp += TransitionViewer.TOP_RIGHT_CORNER_FROM_256;
            }
        }
        if (posX < this.terrain.getSize() - 1 && posY < this.terrain.getSize() - 1) {
            final ICell nextCell = this.terrain.getCell(posY + 1, posX + 1);
            if (nextCell.getCellType() == CellTypes.GROUND) {
                temp += TransitionViewer.BOTTOM_RIGHT_CORNER_FROM_256;
            }
        }
        if (posY < this.terrain.getSize() - 1) {
            final ICell nextCell = this.terrain.getCell(posY + 1, posX - 1);
            if (nextCell.getCellType() == CellTypes.GROUND) {
                temp += TransitionViewer.BOTTOM_LEFT_CORNER_FROM_256;
            }
        }
        return temp;
    }
    
    /**
     * Returns the value corresponding to the neighbor of same type which are connected
     * on the edges. It has 16 possibilities given by the following table.<code><br>
     *        | 1 |   <br>
     *     ---+---+--- <br>
     * &nbsp2 |   | 8 <br>
     *     ---+---+---<br>
     *        | 4 |   <br>
     * </code>
     *
     * @param posY
     *            cell x position.
     * @param posX
     *            cell y position.
     * @param currentCellType
     *            cell type.
     * @param iHeight
     *            cell height.
     * @return transition value, if one of the cells are not of the same type, it returns 0.
     */
    private int computeTransistionEdgeSameType(final int posY, final int posX, final int iHeight,
        final CellTypes currentCellType) {
        int temp = 0;
        if (posY > 0) {
            final ICell nextCell = this.terrain.getCell(posY - 1, posX);
            if (nextCell.getHeight() < iHeight) {
                temp += TransitionViewer.TOP_EDGE;
            }
            if (nextCell.getCellType() != currentCellType) {
                return 0;
            }
        }
        if (posX < this.terrain.getSize() - 1) {
            final ICell nextCell = this.terrain.getCell(posY, posX + 1);
            if (nextCell.getHeight() < iHeight) {
                temp += TransitionViewer.RIGHT_EDGE;
            }
            if (nextCell.getCellType() != currentCellType) {
                return 0;
            }
        }
        if (posY < this.terrain.getSize() - 1) {
            final ICell nextCell = this.terrain.getCell(posY + 1, posX);
            if (nextCell.getHeight() < iHeight) {
                temp += TransitionViewer.BOTTOM_EDGE;
            }
            if (nextCell.getCellType() != currentCellType) {
                return 0;
            }
        }
        if (posX > 0) {
            final ICell nextCell = this.terrain.getCell(posY, posX - 1);
            if (nextCell.getHeight() < iHeight) {
                temp += TransitionViewer.LEFT_EDGE;
            }
            if (nextCell.getCellType() != currentCellType) {
                return 0;
            }
        }
        return temp;
    }
    
    /**
     * Method that computes which transition texture matches given cell and sets the number for the cell.
     *
     * Sea-Ground is computed with 256 possibilities.
     * Sea-Sea is computed with 16 possibilities.
     */
    public void doAutoTransition() {
        final int mapHeight = this.terrain.getSize();
        final int mapWidth = this.terrain.getSize();
        for (int posY = 0; posY < mapHeight; posY++) {
            for (int posX = 0; posX < mapWidth; posX++) {
                final ICell cell = this.terrain.getCell(posY, posX);
                final int height = cell.getHeight();
                if (cell.getCellType().equals(CellTypes.SEA)) {
                    final ICell currentCell = this.terrain.getCell(posY, posX);
                    //for sea-ground height 7 and 8
                    if (currentCell.getHeight() == 8 || currentCell.getHeight() == 7) {
                        int sum = 0;
                        sum += this.computeTransistionDifferentType(posY, posX, currentCell.getCellType());
                        ((SeaCell) cell).setTransitionTexture("GROUND", sum);
                    } else {
                        int sum = 0;
                        sum += this.computeTransistionEdgeSameType(posY, posX, height, currentCell.getCellType());
                        ((SeaCell) cell).setTransitionTexture("SEA", sum);
                    }
                }
            }
        }
        //for sea-sea height 7 and 8
        for (int posY = 0; posY < mapHeight; posY++) {
            for (int posX = 0; posX < mapWidth; posX++) {
                final ICell cell = this.terrain.getCell(posY, posX);
                final int height = cell.getHeight();
                if (cell.getCellType().equals(CellTypes.SEA)) {
                    final ICell currentCell = this.terrain.getCell(posY, posX);
                    if ((currentCell.getHeight() == 8 || currentCell.getHeight() == 7)
                        && ((SeaCell) currentCell).getTransition() == 0) {
                        int sum = 0;
                        sum += this.computeTransistionEdgeSameType(posY, posX, height, currentCell.getCellType());
                        ((SeaCell) cell).setTransitionTexture("SEA", sum);
                    }
                }
            }
        }
    }
    
    /**
     * Renders the ground-sea and water-water transitions.
     *
     * @param graphics
     *            The graphics to draw on.
     * @param coordX
     *            The x coordinate of the cell on screen.
     * @param coordY
     *            The y coordinate of the cell on screen.
     * @param cell
     *            The cell to draw on screen.
     */
    public void render(final Graphics graphics, final float coordX, final float coordY, final ICell cell) {
        Image texture;
        if (cell.getCellType().equals(CellTypes.SEA) && ((SeaCell) cell).getTransition() != 0) {
            texture = this.resourcesLoader.getImage(((SeaCell) cell).getTransitionTextureName());
            texture.draw(coordX, coordY - this.coordinatesManager.getHeightOffset(cell),
                this.coordinatesManager.getScale());
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void reload() {
        this.terrain = Game.getInstance().getWorld().getTerrain();
    }
}
