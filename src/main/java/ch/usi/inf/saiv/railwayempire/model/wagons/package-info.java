/**
 * Provides the application with objects which are treated train components. Wagons are transporting wares. Also
 * wagons are train components which are being pulled by locomotives.
 *
 * AbstrackWagon is an abstract class which provides the basic functionality shared between all the wagons.
 *
 * CoalWagon is transporting only coal.
 *
 * FoodWagon is transporting only food.
 *
 * IronWagon is transporting only iron.
 *
 * MailWagon is transporting only mail.
 *
 * OilWagon is transporting only oil.
 *
 * PassengersWagon is transporting only people.
 *
 * RockWagon is transporting only rock.
 *
 * WoodWagon is transporting only wood.
 */
package ch.usi.inf.saiv.railwayempire.model.wagons;