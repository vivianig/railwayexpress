package ch.usi.inf.saiv.railwayempire.gui.elements.center;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.controllers.CenterPanelsController;
import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractGuiComponent;
import ch.usi.inf.saiv.railwayempire.gui.elements.IGuiComponent;

/**
 * Center panel, holds the different panels that can appear here.
 */
public final class CenterPanel extends AbstractGuiComponent {
    /**
     * Current active panel.
     */
    private IGuiComponent currentPanel;
    
    /**
     * Constructor.
     * 
     * @param rectangle
     *            shape.
     * @param centerPanelsController
     *            center panels controller.
     */
    public CenterPanel(final Rectangle rectangle,
        final CenterPanelsController centerPanelsController) {
        
        super(rectangle);
        
        centerPanelsController.initCenter(rectangle, this);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        graphics.setClip((Rectangle) this.getShape());
        this.currentPanel.render(container, graphics);
        graphics.clearClip();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final int button, final int coordX, final int coordY, final int clickCount) {
        this.currentPanel.mouseClicked(button, coordX, coordY, clickCount);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final int button, final int coordX, final int coordY) {
        this.currentPanel.mousePressed(button, coordX, coordY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final int button, final int coordX, final int coordY) {
        this.currentPanel.mouseReleased(button, coordX, coordY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        this.currentPanel.mouseDragged(oldX, oldY, newX, newY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        this.currentPanel.mouseMoved(oldX, oldY, newX, newY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseWheelMoved(final int charge, final int coordX, final int coordY) {
        this.currentPanel.mouseWheelMoved(charge, coordX, coordY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void keyPressed(final int key, final char character) {
        this.currentPanel.keyPressed(key, character);
    }
    
    /**
     * Get current active center panel.
     * 
     * @return active panel.
     */
    public IGuiComponent getCurrentPanel() {
        return this.currentPanel;
    }
    
    /**
     * Set the current active center panel.
     * 
     * @param currentPanel
     *            new current panel.
     */
    public void setCurrentPanel(final IGuiComponent currentPanel) {
        this.currentPanel = currentPanel;
    }
}
