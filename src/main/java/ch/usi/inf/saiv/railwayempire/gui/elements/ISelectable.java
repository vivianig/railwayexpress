package ch.usi.inf.saiv.railwayempire.gui.elements;

/**
 * Defines the behavior of an object with selectable components.
 * 
 * @param <T>
 *            The type of the selectable components.
 * 
 */
public interface ISelectable<T> {
    
    /**
     * Select the current instance.
     * 
     * Set the instance on which the method is called as the selected instance.
     */
    void select();
    
    /**
     * Unselect the currently selected component.
     * 
     * This set the currently selected component to <code>null</code>.
     */
    void unselect();
    
    /**
     * Get the selected instance of the class.
     * 
     * @return The currently selected instance of the class or <code>null</code> if no instance exists/is selected
     */
    T getSelected();
    
    /**
     * Indicates if the current instance is the selected instance or not.
     * 
     * @return <code>true</code> if the instance is the selected instance, <code>false</code> otherwise.
     */
    boolean isSelected();
    
    /**
     * This method should be responsible to notify all the observers of selection.
     * 
     * It should be called in {@link #select()} and on each observer, it should call
     * {@link ISelectableObserver#notifySelected(ISelectable selected)} and pass itself.
     */
    void notifyObserversSelected();
    
    /**
     * This method should be responsible to notify all the observers of unselection.
     * 
     * It should be called in {@link #unselect()} and on each observer, it should call
     * {@link ISelectableObserver#notifyUnSelected()}.
     */
    void notifyObserverUnselected();
    
    /**
     * Register a new observer to be notified when a component is selected.
     * 
     * @param observer
     *            The observer to be notified
     */
    void registerObserver(ISelectableObserver<T> observer);
    
    /**
     * Unregister an observer from the selection notifications
     * 
     * @param observer
     *            The observer to unregister.
     */
    void unregisterObserver(ISelectableObserver<T> observer);
}
