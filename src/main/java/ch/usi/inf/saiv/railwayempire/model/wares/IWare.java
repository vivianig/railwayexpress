package ch.usi.inf.saiv.railwayempire.model.wares;


/**
 * Interface for the transportable goods.
 */
public interface IWare {

    /**
     * Returns the type of the ware.
     *
     * @return The type of the ware.
     */
    WareTypes getType();

    /**
     * Returns the name of the ware type.
     *
     * @return The name.
     */
    String getTypeName();

    /**
     * Return the quantity.
     *
     * @return quantity.
     */
    double getQuantity();

    /**
     * Add a certain quantity of the ware..
     *
     * @param toAdd
     *         The amount to add.
     */
    void add(final double toAdd);

    /**
     * Remove a quantity of the ware.
     *
     * @param toRemove
     *         The amount to remove.
     */
    void remove(final double toRemove);

    /**
     * Remove everything.
     */
    void clear();

    /**
     * Returns the value of 1 unit of the given ware.
     *
     * @return The value of 1 unit.
     */
    int getValue();
}
