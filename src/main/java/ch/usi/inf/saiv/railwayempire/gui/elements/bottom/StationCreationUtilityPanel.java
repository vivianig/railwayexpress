package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.geom.Rectangle;

import ch.usi.inf.saiv.railwayempire.controllers.GameModes;
import ch.usi.inf.saiv.railwayempire.controllers.GameModesController;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * A panel for station utilities.
 */
public final class StationCreationUtilityPanel extends AbstractWorldInfoUtilityPanel {
    
    /**
     * Margin between buttons
     */
    private final static int BUTTON_MARGIN = 10;
    
    /**
     * Create the panel.
     * 
     * @param rectangle
     *            rectangle.
     * @param modesController
     *            modes controller.
     */
    public StationCreationUtilityPanel(final Rectangle rectangle, final GameModesController modesController) {
        super(rectangle);
        
        final Button smallStation = new Button(
            ResourcesLoader.getInstance().getImage("STATION_SMALL_BUTTON"),
            ResourcesLoader.getInstance().getImage("STATION_SMALL_BUTTON_HOVER"),
            ResourcesLoader.getInstance().getImage("STATION_SMALL_BUTTON_PRESSED"),
            this.getButtonX(1), this.getY());
        
        smallStation.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                modesController.enterMode(GameModes.STATION_SMALL);
            }
        });
        this.add(smallStation);
        
        final Button mediumStation = new Button(
            ResourcesLoader.getInstance().getImage("STATION_MEDIUM_BUTTON"),
            ResourcesLoader.getInstance().getImage("STATION_MEDIUM_BUTTON_HOVER"),
            ResourcesLoader.getInstance().getImage("STATION_MEDIUM_BUTTON_PRESSED"),
            this.getButtonX(2), this.getY());

        mediumStation.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                modesController.enterMode(GameModes.STATION_MEDIUM);
            }
        });
        this.add(mediumStation);
        
        final Button largeStation = new Button(
            ResourcesLoader.getInstance().getImage("STATION_LARGE_BUTTON"),
            ResourcesLoader.getInstance().getImage("STATION_LARGE_BUTTON_HOVER"),
            ResourcesLoader.getInstance().getImage("STATION_LARGE_BUTTON_PRESSED"),
            this.getButtonX(3), this.getY());
        
        largeStation.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                modesController.enterMode(GameModes.STATION_LARGE);
            }
        });
        this.add(largeStation);
    }
    
    /**
     * Returns the position of a button given its ordinal.
     * 
     * @param buttonNumber
     *            The ordinal of the button.
     * @return The position of the button.
     */
    private int getButtonX(final int buttonNumber) {
        final int buttonWidth = ResourcesLoader.getInstance().getImage("STATION_SMALL_BUTTON").getWidth();
        return this.getX() + (buttonWidth + StationCreationUtilityPanel.BUTTON_MARGIN) * (buttonNumber - 1);
    }
}
