package ch.usi.inf.saiv.railwayempire.utilities;


/**
 * Interface used to notify objects to load everything again.
 */
public interface IPersistent {
    
    /**
     * Reload fields of the object.
     */
    void reload();
}
