/**
 * Provides the application whit the functionality of creating and storing tracks and cells.
 *
 * Graph is used for storing the tracks. Based on dijkstra algorithm each train know its next track in order
 * to execute the given route.
 *
 * Matrix is 2 dimensional matrix which stores the information about the cells. Each matrix is supposed to be full,
 * because it stores the information of the terrain. Based on this each cell can be placed properly in the world.
 */
package ch.usi.inf.saiv.railwayempire.model.datastructures;