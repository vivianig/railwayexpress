/**
 * Displays the information by drawing everything that is happening in the world.
 *
 * CoordinatesManager in a way works as a camera that is viewing the world from the top. If anything has to be draw,
 * first it needs to know it the coordinates of the object is in view distance.
 *
 * IViewer is an interface which sets that every viewer must have its render method for drawing.
 *
 * LogViewer display the information by drawing text on the left side of the screen. If something is happening in the world
 * the player is being notified through this LogViewer by drawing important notifications.
 *
 * TrainViewer takes care of drawing train. Both locomotive and wagons are train components of the whole train so they are drawn here.
 *
 * TransitionViewer takes care of computing the transition texture for each tile. When it has been computed, it draws the textures
 * on top of each cell texture.
 *
 * WorldViewer is the top most layer of viewers. It takes care of which thing has to be draw and in which sequence. When it is drawing,
 * it will say when to draw trains to TrainViewer and when to draw transition textures to TransitionViewer.
 */
package ch.usi.inf.saiv.railwayempire.gui.viewers;