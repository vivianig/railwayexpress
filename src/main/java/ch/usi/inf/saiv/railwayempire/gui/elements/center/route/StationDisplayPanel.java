package ch.usi.inf.saiv.railwayempire.gui.elements.center.route;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractPanel;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.utilities.GraphicsUtils;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * A panel to display a station in a route.
 */
public abstract class StationDisplayPanel extends AbstractPanel {
    
    /**
     * The margin between the elements.
     */
    private static final int INTERNAL_MARGIN = 5;
    /**
     * The additional top margin for the text.
     */
    private static final int TEXT_TOP_MARGIN = 10;
    /**
     * The station this panel represents.
     */
    private final Station station;
    
    /**
     * Creates the panel for the station.
     * 
     * @param rectangle
     *            The rectangle in which to render the panel.
     * @param displayStation
     *            The station to display.
     */
    public StationDisplayPanel(final Rectangle rectangle, final Station displayStation) {
        
        super(rectangle);
        this.station = displayStation;
        this.setBackground(ResourcesLoader.getInstance().getImage("BACKGROUND"));
    }
    
    /**
     * Get the internal margin of the panel.
     * 
     * @return The internal margin of the panel.
     */
    protected static int getInternalMargin() {
        return StationDisplayPanel.INTERNAL_MARGIN;
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        super.render(container, graphics);
        
        final String[] name = this.station.getName().trim().split("\\s+-\\s+");
        final String city = name[0];
        final String extension = name[1];
        
        GraphicsUtils.drawShadowText(graphics,
            city, this.getX() + this.getTextXOffset(),
            this.getY() + StationDisplayPanel.INTERNAL_MARGIN + StationDisplayPanel.TEXT_TOP_MARGIN);
        GraphicsUtils.drawShadowText(graphics,
            extension, this.getX() + this.getTextXOffset(),
            this.getY() + StationDisplayPanel.INTERNAL_MARGIN
                * 2
                + StationDisplayPanel.TEXT_TOP_MARGIN
                + graphics.getFont().getLineHeight());
        
        graphics.setColor(new Color(0, 0, 0, 0.3f));
        graphics.drawRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
        graphics.drawRect(this.getX() + 1, this.getY() + 1, this.getWidth() - 2, this.getHeight() - 2);
    }
    
    /**
     * Get the station represented by this panel.
     * 
     * @return The station represented by this panel.
     */
    public Station getStation() {
        return this.station;
    }
    
    /**
     * The x offset at which to draw the text
     * 
     * @return
     */
    abstract protected float getTextXOffset();
}
