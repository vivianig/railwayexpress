package ch.usi.inf.saiv.railwayempire.utilities;

import java.awt.Color;

/**
 * System constants.
 */
public final class SystemConstants {
    
    /**
     * Null object to check for instantiation/existence.
     */
    public static final Object NULL = null;
    /**
     * Zero integer.
     */
    public static final int ZERO = 0;
    /**
     * One integer.
     */
    public static final int ONE = 1;
    /**
     * Two integer.
     */
    public static final int TWO = 2;
    /**
     * Three integer.
     */
    public static final int THREE = 3;
    /**
     * The meaning of life.
     */
    public static final int FORTY_TWO = 42;
    /**
     * The tolerance in pixels for the scrolling of the screen.
     */
    public static final int SCROLLING_TOLERANCE = 8;
    /**
     * The scrolling speed in pixels per second.
     */
    public static final int MOUSE_SCROLLING_SPEED = 1200;
    /**
     * Keyboard Scrolling speed.
     */
    public static final int KEYBOARD_SCROLLING_SPEED = 1000;
    /**
     * The height of the bottom pane during the game.
     */
    public static final int BOTTOM_PANE_HEIGHT = 180;
    /**
     * The height of the bottom pane during the game.
     */
    public static final int RIGHT_PANE_WIDTH = 300;
    /**
     * The multiplier factor for the tile height.
     */
    public static final int TILE_HEIGHT_MULTIPLIER = 10;
    /**
     * The default scale (zoom).
     */
    public static final float DEFAULT_SCALE = 0.2f;
    /**
     * The maximum scale amount.
     */
    public static final float MAX_SCALE = 1;
    /**
     * The minimum scale amount.
     */
    public static final float MIN_SCALE = 0.05f;
    /**
     * The scale factor applied when zooming.
     */
    public static final float SCALE_FACTOR = 1.05f;
    /**
     * int code for left mouse button.
     */
    public static final int MOUSE_BUTTON_LEFT = 0;
    /**
     * int code for right mouse button.
     */
    public static final int MOUSE_BUTTON_RIGHT = 1;
    /**
     * To enable normal logging.
     */
    public static final boolean LOG_OUT = true;
    /**
     * To enable exception logging.
     */
    public static final boolean LOG_EXCEPTION = true;
    /**
     * To enable error logging.
     */
    public static final boolean LOG_ERROR = true;
    /**
     * The mouse button to select. (Left button)
     */
    public static final int SELECT_BUTTON = 0;
    /**
     * The mouse button to unselect. (Right button)
     */
    public static final int UNSELECT_BUTTON = 1;
    /**
     * Save game path.
     */
    public static final String SAVE_PATH = "saves/";
    /**
     * Font size.
     */
    public static final float FONT_SIZE = 14f;
    
    /**
     * Large Font size.
     */
    public static final float LARGE_FONT_SIZE = 20f;
    
    /**
     * Standard custom font path.
     */
    public static final String FONT_PATH = "fonts/ubuntu/Ubuntu-M.ttf";
    /**
     * Font color.
     */
    public static final Color FONT_COLOR = Color.WHITE;
    
    /**
     * Default empty constructor.
     */
    public SystemConstants() {
    }
}
