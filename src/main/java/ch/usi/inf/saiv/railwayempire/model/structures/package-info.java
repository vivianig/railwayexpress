/**
 * Provides the application with objects which are treated as a structure.
 *
 * AbstrackStructure is an abstract class  which provides the basic functionality shared between all structures.
 *
 * Forest is a structure that represents trees in one cell.
 *
 * IStationManagerObserver is a design patter interface for observing the actions of train manager.
 *
 * IStructure provides the interface of structure hierarchy.
 *
 * Station is a structure that operates with production sites and trains. It has the functionality to trade wares
 * between production site and train. It also provides the option of being a node in a route.
 *
 * StationManager manages the station.
 *
 * StationSizes define the size of each station.
 *
 * StructureTypes define the type of each structure.
 *
 * Track is the main structure for the train. Track provides the functionality for trains to move from point a to point b.
 *
 * TrackTypes define the type of each track.
 */
package ch.usi.inf.saiv.railwayempire.model.structures;