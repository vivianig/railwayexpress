package ch.usi.inf.saiv.railwayempire.utilities;

import java.io.Serializable;


/**
 * Represents an ordered pair.
 */
public final class Pair<A, B> implements Serializable {

    /**
     * Serial
     */
    private static final long serialVersionUID = -2266723336239609185L;
    /**
     * The first element in the pair.
     */
    private final A first;
    /**
     * The second element in the pair.
     */
    private final B second;

    /**
     * The constructor for the pair.
     * @param newFirst The first element in the pair.
     * @param newSecond The second element in the pair.
     */
    public Pair(final A newFirst, final B newSecond) {
        this.first = newFirst;
        this.second = newSecond;
    }

    /**
     * Getter for the first element of the pair.
     *
     * @return The first element.
     */
    public A getFirst() {
        return this.first;
    }

    /**
     * Getter for the second element of the pair.
     *
     * @return The second element.
     */
    public B getSecond() {
        return this.second;
    }
}
