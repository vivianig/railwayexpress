package ch.usi.inf.saiv.railwayempire.gui.states;


import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import ch.usi.inf.saiv.railwayempire.gui.GameState;
import ch.usi.inf.saiv.railwayempire.gui.elements.EndGamePanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.EndScorePanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.SimplePanel;
import ch.usi.inf.saiv.railwayempire.utilities.serialization.Highscores;


/**
 * Represents the state in which the player can see the points hew gained.
 */
public final class EndgameState extends BasicGameState {
    
    /**
     * Panel shown.
     */
    private SimplePanel panel;
    
    /**
     * The container.
     */
    private GameContainer container;
    
    /**
     * Check if the game is ready to exit.
     */
    private boolean exit;
    
    /**
     * {@inheritDoc}
     */
    @Override
    public int getID() {
        return GameState.ENDGAME.ordinal();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void keyReleased(final int key, final char c) {
        if (this.exit) {
            this.container.exit();
        } else {
            this.panel = new EndScorePanel(new Rectangle(0, 0, this.container.getWidth(), this.container.getHeight()),
                this.container);
            this.exit = true;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void init(final GameContainer gameContainer, final StateBasedGame stateBasedGame) throws SlickException {
        this.container = gameContainer;
        this.panel = new EndGamePanel(new Rectangle(0, 0, gameContainer.getWidth(), gameContainer.getHeight()),
            gameContainer);
        this.container = gameContainer;
        this.exit = false;
        
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void enter(final GameContainer container, final StateBasedGame game) throws SlickException {
        super.enter(container, game);
        Highscores.getInstance().addScore();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GameContainer gameContainer, final StateBasedGame stateBasedGame, final Graphics graphics)
        throws SlickException {
        this.panel.render(gameContainer, graphics);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void update(final GameContainer gameContainer, final StateBasedGame stateBasedGame, final int i)
        throws SlickException {
    }
}
