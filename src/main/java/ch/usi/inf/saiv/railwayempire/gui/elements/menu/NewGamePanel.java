package ch.usi.inf.saiv.railwayempire.gui.elements.menu;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;
import org.newdawn.slick.gui.TextField;

import ch.usi.inf.saiv.railwayempire.controllers.MainMenuController;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.Label;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.elements.SimplePanel;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils.Alignment;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Panel for the new game sub menu
 */
public final class NewGamePanel extends SimplePanel {
    /**
     * Opaque color.
     */
    private static final Color FADECOLOR = new Color(0, 0, 0, .5f);
    /**
     * Width for the items to be drawn.
     */
    private static final int ITEMS_WIDTH = 200;
    /**
     * Height for the items to be drawn.
     */
    private static final int ITEMS_HEIGHT = 25;
    /**
     * Distance between the buttons and the elements.
     */
    private static final int BUTTON_DISTANCE = 10;
    // comment.
    private static final int CENTER_SPACE = 20;
    // TODO: comment.
    private static final int VERTICAL_DISTANCE = 20;
    /**
     * Text field for the player name.
     */
    private final TextField playerNameInput;
    /**
     * Text field for the company name.
     */
    private final TextField companyNameInput;
    /**
     * New game button.
     */
    private final Button newGameButton;
    /**
     * Right arrow button.
     */
    private final Button rigthButton;
    /**
     * Number of the logo selected.
     */
    private int logoNumber;
    /**
     * Current displayed logo.
     */
    private Image logo;
    /**
     * Current displayed logo id.
     */
    private String logoId;

    /**
     * Instantiate a new "new game panel".
     *
     * @param newShape
     *            The shape of the panel.
     * @param mainMenuController
     *            The controller handling the panel.
     * @param container
     *            The container of the panel.
     */
    public NewGamePanel(final Shape newShape, final MainMenuController mainMenuController, final GameContainer container) {
        super(newShape);

        this.logoNumber = 1;
        this.logoId = "COMPANY_LOGO_" + this.logoNumber;
        this.logo = ResourcesLoader.getInstance().getImage(this.logoId);

        final int labelsX = container.getWidth() / 2 - NewGamePanel.CENTER_SPACE / 2 - NewGamePanel.ITEMS_WIDTH;
        final int textFieldsX = container.getWidth() / 2 + NewGamePanel.CENTER_SPACE / 2;
        int currentY = this.getY() + this.getHeight() / 3;

        // Player name row.
        this.add(new Label(labelsX,
                currentY,
                NewGamePanel.ITEMS_WIDTH,
 NewGamePanel.ITEMS_HEIGHT,
                "Player name",
                Alignment.RIGHT));
        this.playerNameInput = new TextField(container,
                FontUtils.FONT,
                textFieldsX,
                currentY,
                NewGamePanel.ITEMS_WIDTH,
 NewGamePanel.ITEMS_HEIGHT) {
            @Override
            public void keyPressed(final int key, final char c) {
                super.keyPressed(key, c);
                NewGamePanel.this.textFieldKeyPressed();
            }
        };
        this.playerNameInput.setText("John Geek");
        this.playerNameInput.setTextColor(Color.white);

        currentY += this.playerNameInput.getHeight();
        currentY += NewGamePanel.VERTICAL_DISTANCE;

        // Company name row.
        this.add(new Label(labelsX,
                currentY,
                NewGamePanel.ITEMS_WIDTH,
 NewGamePanel.ITEMS_HEIGHT,
                "Company name",
                Alignment.RIGHT));
        this.companyNameInput = new TextField(container,
                FontUtils.FONT,
                textFieldsX,
                currentY,
                NewGamePanel.ITEMS_WIDTH,
 NewGamePanel.ITEMS_HEIGHT) {
            @Override
            public void keyPressed(final int key, final char c) {
                super.keyPressed(key, c);
                NewGamePanel.this.textFieldKeyPressed();
            }

        };
        this.companyNameInput.setText("Geek's & Co.");
        this.companyNameInput.setTextColor(Color.white);
        this.companyNameInput.setBorderColor(null);

        currentY += this.companyNameInput.getHeight();
        currentY += NewGamePanel.VERTICAL_DISTANCE;

        // Company icon row.
        this.add(new Label(labelsX,
                currentY,
                NewGamePanel.ITEMS_WIDTH,
 NewGamePanel.ITEMS_HEIGHT,
                "Company Logo",
                Alignment.RIGHT));


        final Image rigthButtonImage = ResourcesLoader.getInstance().getImage("PREV_ARROW_BUTTON");
        final Image leftButtonImage = ResourcesLoader.getInstance().getImage("NEXT_ARROW_BUTTON");
        this.rigthButton = new Button(rigthButtonImage, ResourcesLoader.getInstance().getImage(
                "PREV_ARROW_BUTTON_HOVER"), ResourcesLoader.getInstance().getImage("PREV_ARROW_BUTTON_PRESSED"),
                textFieldsX, currentY + this.logo.getHeight() / 2 - rigthButtonImage.getHeight() / 2);
        final Button leftButton = new Button(leftButtonImage, ResourcesLoader.getInstance().getImage(
                "NEXT_ARROW_BUTTON_HOVER"), ResourcesLoader.getInstance().getImage("NEXT_ARROW_BUTTON_PRESSED"),
                textFieldsX + 2 * NewGamePanel.BUTTON_DISTANCE + this.rigthButton.getWidth() + this.logo.getWidth(), currentY
                + this.logo.getHeight() / 2 - leftButtonImage.getHeight() / 2);

        this.rigthButton.setListener(new MouseListener() {

            @Override
            public void actionPerformed(final Button button) {
                if (NewGamePanel.this.logoNumber == 1) {
                    NewGamePanel.this.logoNumber = 75;
                } else {
                    NewGamePanel.this.logoNumber--;
                }
                NewGamePanel.this.logoId = "COMPANY_LOGO_" + NewGamePanel.this.logoNumber;
                NewGamePanel.this.logo = ResourcesLoader.getInstance().getImage(
                        NewGamePanel.this.logoId);
            }
        });

        leftButton.setListener(new MouseListener() {

            @Override
            public void actionPerformed(final Button button) {
                if(NewGamePanel.this.logoNumber == 75) {
                    NewGamePanel.this.logoNumber = 1;
                } else {
                    NewGamePanel.this.logoNumber++;
                }
                NewGamePanel.this.logoId = "COMPANY_LOGO_" + NewGamePanel.this.logoNumber;
                NewGamePanel.this.logo = ResourcesLoader.getInstance().getImage(
                        NewGamePanel.this.logoId);
            }
        });

        this.add(this.rigthButton);
        this.add(leftButton);

        currentY += this.logo.getHeight();
        currentY += NewGamePanel.VERTICAL_DISTANCE;

        this.playerNameInput.setFocus(true);

        final Image newGameButtonImage = ResourcesLoader.getInstance().getImage("NEW_GAME_BUTTON");
        final Image cancelImage = ResourcesLoader.getInstance().getImage("CANCEL_BUTTON");

        // align right of new game to right of textfields.
        this.newGameButton = new Button(newGameButtonImage,
                ResourcesLoader.getInstance().getImage("NEW_GAME_BUTTON_HOVER"),
                ResourcesLoader.getInstance().getImage("NEW_GAME_BUTTON_PRESSED"),
                ResourcesLoader.getInstance().getImage("NEW_GAME_BUTTON_DISABLED"),
                textFieldsX + NewGamePanel.ITEMS_WIDTH - newGameButtonImage.getWidth(),
                currentY);
        final Button cancelButton = new Button(cancelImage,
                ResourcesLoader.getInstance().getImage("CANCEL_BUTTON_HOVER"),
                ResourcesLoader.getInstance().getImage("CANCEL_BUTTON_PRESSED"),
                this.newGameButton.getX() - NewGamePanel.BUTTON_DISTANCE - cancelImage.getWidth(),
                currentY);

        this.newGameButton.setListener(new MouseListener() {

            @Override
            public void actionPerformed(final Button button) {
                Game.getInstance().getPlayer().setPlayerName(NewGamePanel.this.playerNameInput.getText());
                Game.getInstance().getPlayer().setCompanyName(NewGamePanel.this.companyNameInput.getText());
                Game.getInstance().getPlayer().setLogo(NewGamePanel.this.logoId);
                mainMenuController.setMainPanel();
                mainMenuController.newGame();
            }
        });

        cancelButton.setListener(new MouseListener() {

            @Override
            public void actionPerformed(final Button button) {
                mainMenuController.setMainPanel();

            }
        });

        this.add(this.newGameButton);
        this.add(cancelButton);

        this.setBackground(NewGamePanel.FADECOLOR);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        super.render(container, graphics);
        graphics.setColor(Color.white);
        this.playerNameInput.render(container, graphics);
        this.companyNameInput.render(container, graphics);
        graphics.drawImage(this.logo, this.rigthButton.getX() + this.rigthButton.getWidth() + NewGamePanel.BUTTON_DISTANCE,
                this.rigthButton.getY() - this.logo.getHeight() / 2 + this.rigthButton.getHeight() / 2);
    }

    /**
     * Handler to disable or enable the new game button.
     */
    private void textFieldKeyPressed() {
        if (this.playerNameInput.getText().isEmpty() || this.companyNameInput.getText().isEmpty()) {
            this.newGameButton.disableButton();
        } else {
            this.newGameButton.enableButton();
        }
    }
}
