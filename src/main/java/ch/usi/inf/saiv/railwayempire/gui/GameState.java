package ch.usi.inf.saiv.railwayempire.gui;


/**
 * Enum representing the states of the game.
 */
public enum GameState {
    /**
     * ID for splash screen.
     */
    SPLASH_SCREEN,
    /**
     * ID for main menu.
     */
    MAIN_MENU,
    /**
     * ID for game state.
     */
    GAMEPLAY,
    /**
     * ID for the end game state.
     */
    ENDGAME
}
