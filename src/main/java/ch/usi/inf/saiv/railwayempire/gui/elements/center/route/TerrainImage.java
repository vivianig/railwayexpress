package ch.usi.inf.saiv.railwayempire.gui.elements.center.route;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.ImageBuffer;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractPanel;
import ch.usi.inf.saiv.railwayempire.model.Terrain;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.utilities.GraphicsUtils;

/**
 * Panel to edit the route of a train.
 */
public class TerrainImage extends AbstractPanel {
    
    /**
     * The terrain from which to generate the image.
     */
    private final Terrain terrain;
    
    /**
     * The scale of the image to generate.
     */
    private final float scale;
    
    /**
     * The generated image of the terrain
     */
    private Image terrainImage;
    
    /**
     * Create a new panel with the image of the terrain.
     * 
     * @param shape
     *            The shape of the panel.
     * @param newTerrain
     *            The terrain for which to generate an image.
     */
    protected TerrainImage(final Shape shape, final Terrain newTerrain) {
        super(shape);
        this.terrain = newTerrain;
        this.scale = this.getHeight() / (this.terrain.getSize() * 1.41f);
        this.terrainImage = this.generateTerrainImage();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        super.render(container, graphics);
        this.terrainImage.drawCentered(this.getX() + this.getWidth() / 2, this.getY() + this.getHeight() / 2);
    }
    
    /**
     * Get the generated image of the terrain.
     * 
     * @return The image of the terrain.
     */
    public Image getImage() {
        return this.terrainImage;
    }
    
    /**
     * Get the scale at which the image is rendered.
     * 
     * @return The scale of the image as a float.
     */
    public float getScale() {
        return this.scale;
    }
    
    /*
     * Generate the image of the terrain.
     */
    private Image generateTerrainImage() {
        final ImageBuffer buffer = new ImageBuffer(this.terrain.getSize(), this.terrain.getSize());
        Color color;
        for (int y = 0; y < this.terrain.getSize(); y++) {
            for (int x = 0; x < this.terrain.getSize(); x++) {
                final ICell current = this.terrain.getCell(x, y);
                
                // Set cell color
                color = GraphicsUtils.getCellColor(current);
                
                buffer.setRGBA(x, y, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
            }
        }
        final Image image = buffer.getImage().getScaledCopy(this.scale);
        image.setRotation(45);
        return image;
    }
    
    /**
     * Regenerate an image of the terrain at it's current state.
     */
    public void updateImage() {
        this.terrainImage = this.generateTerrainImage();
    }
}
