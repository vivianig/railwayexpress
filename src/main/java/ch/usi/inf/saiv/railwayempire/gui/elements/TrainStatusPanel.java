package ch.usi.inf.saiv.railwayempire.gui.elements;

import java.util.HashSet;
import java.util.Set;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.gui.viewers.CoordinatesManager;
import ch.usi.inf.saiv.railwayempire.model.trains.Train;
import ch.usi.inf.saiv.railwayempire.model.trains.TrainState;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils;
import ch.usi.inf.saiv.railwayempire.utilities.Log;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;

/**
 * A train status panel display status information of a train.
 */
public class TrainStatusPanel extends AbstractPanel implements ISelectable<TrainStatusPanel> {
    
    /**
     * The scale of the components of the train.
     */
    private static final float TRAIN_COMPONENTS_SCALE = 0.2f;
    /**
     * The horizontal margin between train components.
     */
    //    private static final float TRAIN_COMPONENTS_X_MARGIN = 2f;
    /**
     * The vertical margin between train components.
     */
    private static final float TRAIN_COMPONENTS_Y_MARGIN = 2f;
    
    /**
     * The instance which is currently selected or <code>null</code> if none is selected.
     */
    private static TrainStatusPanel SELECTED = null;
    
    /**
     * The set of observers to be notified when the panel is selected or deselected.
     */
    private static final Set<ISelectableObserver<TrainStatusPanel>> observers =
        new HashSet<ISelectableObserver<TrainStatusPanel>>();
    
    /**
     * The train to display.
     */
    private final Train train;
    
    private final TrainDisplayPanel trainDisplay;
    
    /**
     * Create a train status display panel of the given dimension (rectangle)
     * 
     * @param rectangle
     *            The dimension of the panel
     * @param newTrain
     *            For demo/debug purposes only.
     */
    public TrainStatusPanel(final Rectangle rectangle, final Train newTrain) {
        super(rectangle);
        this.train = newTrain;
        
        this.trainDisplay = new TrainDisplayPanel(new Rectangle(this.getX() + 20,
            (int) (this.getVerticalOffset(1) + TrainStatusPanel.TRAIN_COMPONENTS_Y_MARGIN),
            this.getWidth() - 40, 70 * TrainStatusPanel.TRAIN_COMPONENTS_SCALE), this.train);
        this.add(this.trainDisplay);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        //Background
        final Image backgroundImage = ResourcesLoader.getInstance()
            .getImage("TRAIN_STATUS_BG" + (this.isSelected() ? "_HIGHLIGHT" : ""));
        graphics.drawImage(backgroundImage,
            this.getX(), this.getY(), this.getX() + this.getWidth(), this.getY() + this.getHeight(),
            0, 0, backgroundImage.getWidth(), backgroundImage.getHeight());
        
        //Train Name
        FontUtils.drawCenter(graphics.getFont(), this.train.getName(),
            this.getX() + 20, (int) this.getVerticalOffset(0), this.getWidth() - 40, Color.black);
        
        //Destination
        FontUtils.drawLeft(graphics.getFont(), (this.train.isParked() ?
            "Warehouse" : "To: " + this.train.getRoute().getCurrentStation().getName()),
            this.getX() + 58, (int) this.getVerticalOffset(2), Color.black);
        
        //State
        if (this.train.getState() == TrainState.MOVING) {
            FontUtils.drawLeft(graphics.getFont(), "Speed: " + this.train.getVelocityString(),
                this.getX() + 58, (int) this.getVerticalOffset(3), Color.black);
        } else {
            FontUtils.drawLeft(graphics.getFont(), FontUtils.capitalizeFirstLetter(this.train.getState().name()),
                this.getX() + 58, (int) this.getVerticalOffset(3), Color.black);
        }
        
        //State Icon
        graphics.drawImage(ResourcesLoader.getInstance().getImage("TRAIN_STATE_" + this.train.getState()),
            this.getX() + 20,
            this.getY() + this.getHeight() - 10 - 28,
            this.getX() + 20 + 28,
            this.getY() + this.getHeight() - 10, 0, 0, 48, 48);
        
        super.render(container, graphics);
    }
    
    /**
     * Returns the vertical offset of the line in the panel.
     * 
     * @param line
     *            The number of the line to be displayed.
     * @return The vertical offset.
     */
    private float getVerticalOffset(final int line) {
        return this.getY() + line * (this.getHeight() - 20) / 4 + 10;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void select() {
        if (null != TrainStatusPanel.SELECTED) {
            TrainStatusPanel.SELECTED.disableTrainScroll();
        }
        TrainStatusPanel.SELECTED = this;
        TrainStatusPanel.SELECTED.enableTrainScroll();
        this.notifyObserversSelected();
        
    }
    
    /**
     * Disable the autoscrolling of the train composition.
     */
    protected void disableTrainScroll() {
        this.trainDisplay.disableAutoScroll();
        
    }
    
    /**
     * Enable the autoscrolling of the train composition.
     */
    private void enableTrainScroll() {
        this.trainDisplay.enableAutoScroll();
        
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void unselect() {
        if (null != TrainStatusPanel.SELECTED) {
            TrainStatusPanel.SELECTED.disableTrainScroll();
            TrainStatusPanel.SELECTED = null;
        }
    }
    
    /**
     * @return The selected instance.
     * @see #getSelected()
     */
    public static TrainStatusPanel getStaticSelected() {
        return TrainStatusPanel.SELECTED;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public TrainStatusPanel getSelected() {
        return TrainStatusPanel.SELECTED;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSelected() {
        return this.getSelected() == this;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void registerObserver(final ISelectableObserver<TrainStatusPanel> observer) {
        TrainStatusPanel.observers.add(observer);
        
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void unregisterObserver(final ISelectableObserver<TrainStatusPanel> observer) {
        TrainStatusPanel.observers.remove(observer);
        
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final int button, final int coordX, final int coordY, final int clickCount) {
        if (clickCount == 2) {
            if (this.train.isParked()) {
                Log.screen("This train is parked, you cannot see it.");
            } else {
                CoordinatesManager.getInstance().centerCameraOn(this.train.getX(), this.train.getY());
            }
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final int button, final int coordX, final int coordY) {
        switch (button) {
            case SystemConstants.SELECT_BUTTON:
                this.select();
                break;
            case SystemConstants.UNSELECT_BUTTON:
                this.notifyObserverUnselected();
            default:
                break;
        }
    }
    
    /**
     * Get the train represented by the panel.
     * 
     * @return THe train represented by the panel.
     */
    public Train getTrain() {
        return this.train;
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void notifyObserversSelected() {
        for (final ISelectableObserver<TrainStatusPanel> observer : TrainStatusPanel.observers) {
            observer.notifySelected(this);
        }
        
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void notifyObserverUnselected() {
        for (final ISelectableObserver<TrainStatusPanel> observer : TrainStatusPanel.observers) {
            observer.notifyUnSelected();
        }
    }
    
}
