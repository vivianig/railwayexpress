package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.geom.Rectangle;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.controllers.GameModesController;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.Label;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.viewers.CoordinatesManager;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Bottom Panel for utility display according to selected station if selected at all.
 */
public final class StationUtilityPanel extends AbstractWorldInfoUtilityPanel {
    
    /**
     * Label width.
     */
    private final static int LABEL_WIDTH = 180;
    /**
     * Label height.
     */
    private final static int LABEL_HEIGHT = 20;
    /**
     * The margin for the logo.
     */
    private final static int IMAGE_MARGIN = 8;
    /**
     * Margin.
     */
    private final static int MARGIN = 5;
    /**
     * Creates the panel for displaying selected locomotive information.
     *
     * @param rectangle
     *            utility rectangle.
     * @param station
     *            selected station.
     * @param modesController
     *            modes controller.
     */
    public StationUtilityPanel(final Rectangle rectangle, final Station station,
        final GameModesController modesController) {
        super(rectangle);
        final ResourcesLoader loader = ResourcesLoader.getInstance();
        
        final Button stationLogo =
            new Button(loader.getImage("STATION_" + station.getSize() + "_BUTTON"),
                loader.getImage("STATION_" + station.getSize() + "_BUTTON_HOVER"),
                loader.getImage("STATION_" + station.getSize() + "_BUTTON_PRESSED"),
                this.getX(),
                this.getY());
        stationLogo.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                CoordinatesManager.getInstance().centerCameraOn(station.getX(), station.getY());
            }
        });
        
        this.add(stationLogo);
        
        this.add(new Label(this.getX() + StationUtilityPanel.MARGIN + stationLogo.getWidth() + 2
            * StationUtilityPanel.IMAGE_MARGIN,
            this.getY(),
            StationUtilityPanel.LABEL_WIDTH,
            StationUtilityPanel.LABEL_HEIGHT,
            station.getName()));
        
        this.add(new Label(this.getX() + StationUtilityPanel.MARGIN + stationLogo.getWidth() + 2
            * StationUtilityPanel.IMAGE_MARGIN,
            this.getY() + StationUtilityPanel.MARGIN + StationUtilityPanel.LABEL_HEIGHT,
            StationUtilityPanel.LABEL_WIDTH,
            StationUtilityPanel.LABEL_HEIGHT,
            station.getCity().getName()));
        
        this.add(new Label(this.getX() + StationUtilityPanel.MARGIN + stationLogo.getWidth() + 2
            * StationUtilityPanel.IMAGE_MARGIN,
            this.getY() + StationUtilityPanel.MARGIN + StationUtilityPanel.LABEL_HEIGHT * 2,
            StationUtilityPanel.LABEL_WIDTH,
            StationUtilityPanel.LABEL_HEIGHT,
            Game.getInstance().getPlayer().getPlayerName()));
        
        this.add(new Label(this.getX() + StationUtilityPanel.MARGIN + stationLogo.getWidth() + 2
            * StationUtilityPanel.IMAGE_MARGIN,
            this.getY() + StationUtilityPanel.MARGIN + StationUtilityPanel.LABEL_HEIGHT * 3,
            StationUtilityPanel.LABEL_WIDTH,
            StationUtilityPanel.LABEL_HEIGHT,
            "Radius: " + station.getRadius()));
        
        this.add(new Label(this.getX() + StationUtilityPanel.MARGIN + stationLogo.getWidth() + 2
            * StationUtilityPanel.IMAGE_MARGIN,
            this.getY() + StationUtilityPanel.MARGIN + StationUtilityPanel.LABEL_HEIGHT * 4,
            StationUtilityPanel.LABEL_WIDTH,
            StationUtilityPanel.LABEL_HEIGHT,
            "Production sites connected: " + station.getProductionSites().size()));
        
        this.add(new Label(this.getX() + StationUtilityPanel.MARGIN + stationLogo.getWidth() + 2
            * StationUtilityPanel.IMAGE_MARGIN,
            this.getY() + StationUtilityPanel.MARGIN + StationUtilityPanel.LABEL_HEIGHT * 5,
            StationUtilityPanel.LABEL_WIDTH,
            StationUtilityPanel.LABEL_HEIGHT,
            "Is station linked: " + station.isLinked()));
        
        this.add(new Label(this.getX() + StationUtilityPanel.MARGIN + stationLogo.getWidth() + 2
            * StationUtilityPanel.IMAGE_MARGIN,
            this.getY() + StationUtilityPanel.MARGIN + StationUtilityPanel.LABEL_HEIGHT * 6,
            StationUtilityPanel.LABEL_WIDTH,
            StationUtilityPanel.LABEL_HEIGHT,
            "Stations connected: " + station.getReachableStations().size()));
    }
}
