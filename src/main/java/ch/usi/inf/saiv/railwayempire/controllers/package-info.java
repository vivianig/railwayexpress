/**
 * The main purpose of this package is to proved controllers for the MVC pattern which is being used in this applications.
 *
 */
package ch.usi.inf.saiv.railwayempire.controllers;