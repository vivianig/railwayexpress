package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.geom.Shape;

/**
 * Info panel for the train route view.
 */
public final class TrainRouteInfoPanel extends AbstractInfoPanel {
    
    /**
     * Constructor is here.
     * 
     * @param shape
     *            Some shape apparently.
     */
    public TrainRouteInfoPanel(final Shape shape) {
        super(shape);
    }
}
