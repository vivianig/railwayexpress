package ch.usi.inf.saiv.railwayempire.utilities.serialization;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.utilities.Log;
import ch.usi.inf.saiv.railwayempire.utilities.Pair;
import ch.usi.inf.saiv.railwayempire.utilities.StatisticsManager;


/**
 * Represents a holder for the highscores.
 */
public final class Highscores implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1666042079253202874L;
    /**
     * The maximum number of stored highscores.
     */
    private static final int HIGHSCORES_LIMIT = 10;
    /**
     * The unique instance of the highscores holder.
     */
    private static final Highscores UNIQUE_INSTANCE = new Highscores();
    /**
     * A list of pairs of the player and their respective scores.
     */
    private final List<Pair<String, Long>> scores;

    /**
     * Instantiates the highscore holder.
     */
    private Highscores() {
        final Highscores highscores = this.loadScores();
        if (null == highscores) {
            this.scores = new ArrayList<Pair<String, Long>>();
        } else {
            this.scores = highscores.getScores();
        }
    }

    /**
     * Loads the highscore object from the file if it exists, otherwise returns null.
     *
     * @return The highscore object or null.
     */
    private Highscores loadScores() {
        Highscores highscores = null;
        try {
            final File highscoresFile = new File("highscores");
            if (!highscoresFile.exists()) {
                return null;
            }
            final FileInputStream fileIn = new FileInputStream(highscoresFile);
            final ObjectInputStream in = new ObjectInputStream(fileIn);
            highscores = (Highscores) in.readObject();
            in.close();
            fileIn.close();
        } catch (final IOException exception) {
            Log.exception(exception);
        } catch (final ClassNotFoundException exception) {
            Log.exception(exception);
        }
        return highscores;
    }

    /**
     * Returns a map from player name to its respective score.
     *
     * @return The map from player name to score.
     */
    public List<Pair<String, Long>> getScores() {
        return this.scores;
    }

    /**
     * Returns the unique instance of the highsocres holder.
     *
     * @return The instance.
     */
    public static Highscores getInstance() {
        return UNIQUE_INSTANCE;
    }

    /**
     * Adds a score to the table. The score is automatically taken from the statistics
     * manager and added only if it is high enough to be an high score.
     */
    public void addScore() {
        final StatisticsManager manager = StatisticsManager.getInstance();
        int index = 0;
        for (final Pair<String, Long> score : this.scores) {
            if (manager.computeScore() > score.getSecond()) {
                break;
            }
            index++;
        }
        if ((this.scores.size() == 0 || index <= this.scores.size()) && index < HIGHSCORES_LIMIT) {
            if (this.scores.size() == HIGHSCORES_LIMIT) {
                this.scores.remove(this.scores.remove(this.scores.size() - 1));
            }
            this.scores.add(index,
                    new Pair<String, Long>(Game.getInstance().getPlayer().getPlayerName(), manager.computeScore()));
        }
        this.saveScores();
    }

    /**
     * Saves the highscore object to its file.
     */
    private void saveScores() {
        try {
            final File highscoresFile = new File("highscores");
            highscoresFile.createNewFile();
            final FileOutputStream fileOut = new FileOutputStream(highscoresFile);
            final ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(this);
            out.close();
            fileOut.close();
        } catch (final IOException exception) {
            Log.exception(exception);
        }
    }
}
